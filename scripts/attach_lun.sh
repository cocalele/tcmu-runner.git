#!/bin/bash
function fatal {
    echo -e "\033[31m$* \033[0m"
    exit 1
}
function info {
    echo -e "\033[32m$* \033[0m"
}

function assert()
{
    local cmd=$*
    echo "Run:$cmd" > /dev/stderr
    eval '${cmd}'
    if [ $? -ne 0 ]; then
        fatal "Failed to run:$cmd"
    fi
}
LUN_NAME=$1
assert iscsiadm -m discovery -t sendtargets -p 172.21.0.13
iscsiadm --mode node --targetname $LUN_NAME --portal 172.21.0.13 -u
assert iscsiadm --mode node --targetname $LUN_NAME --portal 172.21.0.13 --login
info "Attach lun $LUN_NAME succeeded"
