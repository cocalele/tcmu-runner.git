#!/bin/bash
function fatal {
    echo -e "\033[31m$* \033[0m"
    exit 1
}
function info {
    echo -e "\033[32m$* \033[0m"
}

function assert()
{
    local cmd=$*
    echo "Run:$cmd" > /dev/stderr
    eval '${cmd}'
    if [ $? -ne 0 ]; then
        fatal "Failed to run:$cmd"
    fi
}

export PYTHONPATH='/root/rtslib-fb:/root/targetcli-fb'
alias targetcli='python3 /root/targetcli-fb/scripts/targetcli'
## Clean original setting
#targetcli clearconfig confirm=True

# Remove all original config #
assert targetcli /backstores/user:pfbd create cfgstring=test_v1 name=lun_test_v1 size=2G



#targetcli /iscsi create
iscsiname=`targetcli /iscsi create | grep "Created" | head -n1 | awk '{print $3}'`;

iscsiname=${iscsiname%.*}

info "iscsiname: $iscsiname"



# /backstores/block/my_vol1

targetcli /iscsi/${iscsiname}/tpg1/luns create  /backstores/user:pfbd/lun_test_v1
targetcli /iscsi/${iscsiname}/tpg1 set attribute authentication=0 demo_mode_write_protect=0 generate_node_acls=1



targetcli /iscsi/${iscsiname}/tpg1/portals delete 0.0.0.0 3260

[[ $? != 0 ]]  && targetcli /iscsi/${iscsiname}/tpg1/portals/ delete 0.0.0.0 3260 confirm=True



targetcli /iscsi/${iscsiname}/tpg1/portals create 172.21.0.13 3260